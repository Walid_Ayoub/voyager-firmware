Voyager Arduino Firmware for:

1. Kiosk, BagDrop and Duty Free: VoyagerKiosk.ino
   Arduino code that provide a serial API for these equipment to control 
   the voyager unit LEDs
 
 2. Boarding Gate, BGVoyager4.ino
    Arduino code that controls the gater Photoelectric sensors and Gate Motors,
    as well as the LEDs on the voyager unit