int dimmer = 0;
long serialTimer1;
long serialTimer2;
long timeOut = 3000;

String incomingByte;
String command;

boolean isSerial;
boolean isTurnedOff;
boolean isConnected;
boolean isDetected;
boolean isStartSpeach;
boolean isFinalSpeach = false;
boolean isStandBy;

int isCorrectCommand;
int dimTimer;
int counter = 0;
int blinks;
int delayMs;
int standByTimer;

int isRecognised = 0;

boolean isDimUp = true;
int timeStart;
int timeFinish;
int intervalTime;

// Setup PWM pins for RGB
#define R 5
#define B 6
#define G 3
#define kioskGreen 4

#define LG 16
#define RG 20
#define rGate 15
#define gGate 14

#define PE1  A3  //PE1 ENTRY
#define PE2  A6  //PE2 ENTRY 
#define PE5  A5  //PE3 ENTRY
#define PE4  A0  //PE4 ENTRY
#define PE3  A7  //PE5 ENTRY
#define PE6  A2  //PE6 ENTRY
#define PE7  A1  //PE7 ENTRY

#define PE8  19  //PE1 EXIT 
#define PE9  17  //PE2 EXIT 
#define PE10 18  //PE3 EXIT

String gateEntrance = "0000000";
String gateExit = "000";
int entrancePE[] = {PE1, PE2, PE3, PE4, PE5, PE6, PE7};
int exitPE[] = {PE8, PE9, PE10};

int MAXDIM = 220;
int MINDIM = 10;

const int PINGING = 20000;
const int serialTimeOut = 100;

void setup() {

  Serial.begin(115200);
  Serial.setTimeout(100);

  pinMode(R, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(G, OUTPUT);

  pinMode(PE1, INPUT);
  pinMode(PE2, INPUT);
  pinMode(PE3, INPUT);
  pinMode(PE4, INPUT);
  pinMode(PE5, INPUT);
  pinMode(PE6, INPUT);
  pinMode(PE7, INPUT);
  pinMode(PE8, INPUT);
  pinMode(PE9, INPUT);
  pinMode(PE10, INPUT);

  digitalWrite(PE1,LOW);
  digitalWrite(PE2,LOW);
  digitalWrite(PE3,LOW);
  digitalWrite(PE4,LOW);
  digitalWrite(PE5,LOW);
  digitalWrite(PE6,LOW);
  digitalWrite(PE7,LOW);
  digitalWrite(PE8,LOW);
  digitalWrite(PE9,LOW);
  digitalWrite(PE10,LOW);

  pinMode(LG, OUTPUT);
  pinMode(RG, OUTPUT);
  pinMode(rGate, OUTPUT);
  pinMode(gGate, OUTPUT);

  timeStart = millis();
  timeFinish = millis();
  clearFlags();

  digitalWrite(rGate, LOW);
  digitalWrite(gGate, HIGH);

  digitalWrite(LG, LOW);
  digitalWrite(RG, LOW);
}

void loop() {
  ping();
  if (!isSerial) {
    serialTimer2 = millis();
    if (serialTimer2 - serialTimer1 >= PINGING) {
      if (!isTurnedOff) {
        isTurnedOff = true;
        analogWrite(R, 0);
        analogWrite(G, 0);
        analogWrite(B, 0);
      }
      dimLED(R);
      isStandBy = false;
    }
  } else {
    if (command.equals("c\n")) {
      Serial.write("C\n");
      isConnected = true;
      if (!isTurnedOff) {
        isTurnedOff = true;
        analogWrite(R, 0);
        analogWrite(G, 0);
        analogWrite(B, 0);
      }
      blinkLED(R, 1);
      blinkLED(G, 1);
      blinkLED(B, 1);
    }
    if (command.equals("ow\n")) {
      Serial.write("OW\n");
      digitalWrite(LG, LOW);
      digitalWrite(RG, LOW);
      digitalWrite(gGate, HIGH);
      digitalWrite(rGate, LOW);
    }
    if (command.equals("cw\n")) {
      Serial.write("CW\n");
      digitalWrite(LG, HIGH);
      digitalWrite(RG, HIGH);
      digitalWrite(gGate, LOW);
      digitalWrite(rGate, HIGH);
    }
    if (command.equals("g\n")) {
      Serial.write("G");
      Serial.write(",");
      delay(10);
      for (int i = 0; i < 7; i++ ) {
        if (filterPE(entrancePE[i]) == HIGH) {
          Serial.write("1");
        } else {
          Serial.write("0");
        }
        Serial.write(",");
      }
      for (int i = 0; i < 3; i++ ) {
        if (digitalRead(exitPE[i]) == HIGH) {
          Serial.write("1");
        } else {
          Serial.write("0");
        }
        if (i<2) Serial.write(",");
        delay(10);
      }
      Serial.print("\n");
    }
    if (command.equals("f\n")) {
      Serial.write("F\n");
      //Serial.println(micros());
      blinks = 1;
      counter = 0;
      delayMs = 3;
      analogWrite(R, 0);
      analogWrite(G, 0);
      while (counter < blinks) {
        fastDimLED(B, delayMs);
        if (Serial.available()) {
          if (dimmer == MINDIM) {
            break;
          }
        }
      }
    }
    if (command.equals("r\n")) {
      Serial.write("R\n");
      blinks = 1;
      counter = 0;
      delayMs = 3;
      while (counter < blinks) {
        analogWrite(R, 0);
        analogWrite(B, 0);
        fastDimLED(G, delayMs);
        if (dimmer == MAXDIM) {
        }
      }
    }
    if (command.equals("x\n")) {
      Serial.write("X\n");
      blinks = 6;
      counter = 0;
      delayMs = 1;
      while (counter < blinks) {
        analogWrite(G, 0);
        analogWrite(B, 0);
        fastDimLED(R, delayMs);
      }
    }
    if (command.equals("p\n")) {
     
        Serial.write("P\n");
      isStartSpeach = true;
      counter = 0;
      dimmer = 0;
      isDimUp = true;
      delayMs = 2;
      while (!Serial.available()) {
        analogWrite(B, 0);
        analogWrite(B, 0);
        speechLED(G, delayMs);
      }
      while (dimmer > 0) {
        fastDimLED(G, delayMs);
      }
    }
    if (command.equals("e\n")) {
      Serial.write("E\n");
      counter = 0;
      analogWrite(R, 0);
      analogWrite(G, 0);
      analogWrite(B, 0);
      delayMs = 7;
      while (true) {
        dimWhite(delayMs);
        if (Serial.available()) {
          if (dimmer == MINDIM) {
            break;
          }
        }
      }
        
      }
      
    }
    if (command.equals("rc\n")) {
      Serial.write("RC\n");
      blinks = 2;
      counter = 0;
      delayMs = 1;
      while (counter < blinks) {
        analogWrite(R, 0);
        analogWrite(B, 0);
        fastDimLED(G, delayMs);
      }
    }
    if (command.equals("xc\n")) {
      Serial.write("XC\n");
      blinks = 4;
      counter = 0;
      delayMs = 1;
      while (counter < blinks) {
        analogWrite(G, 0);
        analogWrite(B, 0);
        fastDimLED(R, delayMs);
      }
    }
    if (command.equals("k\n")) {
      Serial.write("K\n");
      if (!isStandBy) {
        isStandBy = true;
        standByTimer = millis();
      }
      counter = 0;
      dimmer = 0;
      clearFlags();
    }
    command = "";
  
}

void clearFlags() {
  analogWrite(R, 0);
  analogWrite(G, 0);
  analogWrite(B, 0);
  dimmer = 0;
  isDimUp = true;
  counter = 0;
  MAXDIM = 220;
  MINDIM = 0;
}

void ping() {
  timeFinish = millis();
  intervalTime = timeFinish - timeStart;

  if (intervalTime > PINGING) {
    timeStart = millis();
    isConnected = false;
    isDetected = false;
    isStartSpeach = false;
    isFinalSpeach = false;
    isRecognised = 0;
    isCorrectCommand = 0;
  }
}

void dimLED(int LED) {
  serialEvent();
  MAXDIM = 220;
  MINDIM = 0;
  if (isDimUp) dimUp();
  else dimDown();
  analogWrite(LED, dimmer);
  delay(10);
}

void dimUp() {
  dimmer++;
  if ( dimmer >= MAXDIM ) {
    isDimUp = false;
    counter++;
  }
}

void dimDown() {
  dimmer--;
  if (dimmer <= MINDIM) {
    isDimUp = true;
    counter++;
  }
}

void blinkLED(int LED, int blinks) {
  serialEvent();
  int counter = 0;
  dimmer == 0;
  while (counter < blinks) {
    analogWrite(LED, 0);
    delay(100);
    analogWrite(LED, 220);
    delay(100);
    analogWrite(LED, 0);
    counter++;
  }
  delay(100);
}

void dimWhite(int delayMs) {
  MAXDIM = 220;
  MINDIM = 0;
  analogWrite(R, dimmer);
  analogWrite(B, dimmer);
  analogWrite(G, dimmer);
  if (isDimUp) dimUp();
  else dimDown();
  delay(delayMs);
}


void fastDimLED(int LED, int delayMs) {
  MAXDIM = 220;
  MINDIM = 0;
  analogWrite(LED, dimmer);
  if (isDimUp) dimUp();
  else dimDown();
  delay(delayMs);
}

void speechLED(int LED, int delayMs) {
  serialEvent();
  if (isDimUp) {
    dimUp();
    MINDIM = random(0, 100);
  } else {
    dimDown();
    MAXDIM = random(100, 220);
  }
  analogWrite(LED, dimmer);

  delay(delayMs);
}

void dimDownToColor(int LED, int intensity) {
  serialEvent();
  if (!isDimUp && dimmer > intensity) dimDown();
  analogWrite(LED, dimmer);
  delay(2);
}

void dimUpToColor(int LED) {
  serialEvent();
  MAXDIM = 220;
  MINDIM = 0;
  dimUp();
  analogWrite(LED, dimmer);
  delay(delayMs);
}

int filterPE(int PE){
 counter = 0;
 int PETest = 0;
  while (counter < 1000){
    //delay(1);
    if (digitalRead(PE) == HIGH)PETest ++;
    counter++;
  }

  if (PETest > 900){
    return HIGH; 
  }else{
    return LOW;
  }
 
}

void serialEvent() {
  //isStandBy = false;
  while (Serial.available()) {
    command = Serial.readString();
    isSerial = true;
    serialTimer1 = millis();
    serialTimer2 = millis();
  }
}
